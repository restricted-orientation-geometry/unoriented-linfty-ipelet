Unoriented linfty IPElet
========================

An Ipelet [1] plugin to compute the Voronoi diagram [2] and the Delaunay
triangulation [3] of a set of points in the plane on the infinity metric for
different orientations of the coordinate axis.

Requirements
------------

+ IPE 7.2.7+
+ CGAL 4.14+
+ GMP 6.1.2+

References
----------

- [1] http://ipe.otfried.org/
- [2] https://en.wikipedia.org/wiki/Voronoi_diagram
- [3] https://en.wikipedia.org/wiki/Delaunay_triangulation