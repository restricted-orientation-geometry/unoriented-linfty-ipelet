/*
 * This file is part of unoriented-linfty-ipelet.
 *
 * Copyright 2017 Carlos Alegría Galicia
 *
 * unoriented-linfty-ipelet is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or any later version.
 *
 * unoriented-linfty-ipelet is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with unoriented-linfty-ipelet. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Segment_Delaunay_graph_Linf_filtered_traits_2.h>
#include <CGAL/Segment_Delaunay_graph_Linf_2.h>
#include <CGAL/aff_transformation_tags.h>
#include <CGAL/Aff_transformation_2.h>
#include <CGAL/CGAL_Ipelet_base.h>
#include <CGAL/number_utils.h>

#include <cassert>
#include <ostream>
#include <cmath>
#include <list>

#define IPELET_NAME "Unoriented Linfinity graphs"
#define IPELET_HELP "Help"
#define IPELET_FUNCTION_NO 4

#define IPELET_DELAUNAY "Delaunay"
#define IPELET_DELAUNAY_HELP "Draw the Linfinity-Delaunay triangulation of a set of points"
#define IPELET_DELAUNAY_OPTION 0

#define IPELET_VORONOI "Voronoi"
#define IPELET_VORONOI_HELP "Draw the Linfinity-Voronoi diagram of a set of points"
#define IPELET_VORONOI_OPTION 1

#define IPELET_BISECTOR "Squares"
#define IPELET_BISECTOR_HELP "Draw the Linfinity-Balls of a set of points"
#define IPELET_BISECTOR_OPTION 2


namespace unoriented_linfty
{
  typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;

  //
  // based on svdlinf.cpp
  //
  class Unoriented_Linfty_Geometry_ipelet : public CGAL::Ipelet_base<Kernel,
      IPELET_FUNCTION_NO>
  {
  public:

    // declare the ipelet and its functions (including help message).
    //
    Unoriented_Linfty_Geometry_ipelet () :
	CGAL::Ipelet_base<Kernel, IPELET_FUNCTION_NO> (
	    IPELET_NAME,
	    // functions
	    //
	    new std::string[IPELET_FUNCTION_NO]
	      {IPELET_DELAUNAY, IPELET_VORONOI, IPELET_BISECTOR, IPELET_HELP},
	    // Help messages
	    //
	    new std::string[IPELET_FUNCTION_NO]
	      {IPELET_DELAUNAY_HELP, IPELET_VORONOI_HELP , IPELET_BISECTOR_HELP})
    {
    }

    void
    protected_run (int);

  private:

    typedef CGAL::Segment_Delaunay_graph_Linf_filtered_traits_2<Kernel,CGAL::Field_with_sqrt_tag> Gt;
    typedef CGAL::Segment_Delaunay_graph_Linf_2<Gt> SDG2;
    typedef CGAL::Aff_transformation_2<Kernel> Transformation;

    // Class using stream to get the Voronoi diagram from a triangulation
    //
    struct Stream
    {
      const Transformation &transformation;
      std::list<Ray_2> ray_list;
      std::list<Line_2> line_list;
      std::list<Segment_2> seg_list;


      Stream (const Transformation &t) :
	  transformation (t)
      {
      }

      void
      operator<< (const Ray_2& p)
      {
	ray_list.push_back (
	    Ray_2 (transformation.transform (p.point (0)),
		   transformation.transform (p.point (1))));
      }

      void
      operator<< (const Line_2& p)
      {
	line_list.push_back (
	    Line_2 (transformation.transform (p.point (0)),
		    transformation.transform (p.point (1))));
      }

      void
      operator<< (const Segment_2& p)
      {
	seg_list.push_back (
	    Segment_2 (transformation.transform (p.point (0)),
		       transformation.transform (p.point (1))));
      }
    };

    void
    draw_delaunay_in_ipe (const SDG2& g, const Transformation& inv,
			  const Iso_rectangle_2 &bbox) const;

    void
    draw_voronoi_in_ipe (const SDG2& g, const Transformation& inv,
			 const Iso_rectangle_2 &bbox) const;

    void
    draw_squares_in_ipe (const SDG2& g, const Transformation& inv,
			 const Iso_rectangle_2 &bbox) const;

    template<class DrawingFunction>
      void
      for_each_angle (const std::vector<Point_2> &points,
		      const Iso_rectangle_2 &bbox, DrawingFunction f,
		      double angle_inc) const;

    template<class T, class output_iterator>
      void
      cast_into_seg (const T& obj, const Iso_rectangle_2& bbox,
		     output_iterator out_it) const;

    template<class iterator, class output_iterator>
      void
      cast_into_seg (const iterator begin, const iterator end,
		     const Iso_rectangle_2& bbox, output_iterator out_it) const;
  };

  // ---------------------------------------------------------------------------

  void
  Unoriented_Linfty_Geometry_ipelet::protected_run (int fn)
  {
    //
    // print help message
    //

#ifdef NDEBUG
    std::cerr << "fn = " << fn << std::endl;
#endif
    if (fn == (IPELET_FUNCTION_NO - 1))
      {
	show_help ();
	return;
      }


    //
    // read input
    //

    Iso_rectangle_2 bbox;
    std::vector<Point_2> points;

    bbox = read_active_objects (
	CGAL::dispatch_or_drop_output<Point_2> (std::back_inserter (points)));

    if (points.empty ())
      {
	print_error_message ("No mark selected");
	return;
      }

    // slightly increase the size of the bbox
    //
    Kernel::FT incr_len = 75;
    bbox = Iso_rectangle_2 (
	bbox.min () + Kernel::Vector_2 (-incr_len, -incr_len),
	bbox.max () + Kernel::Vector_2 (incr_len, incr_len));

#ifdef NDEBUG
    std::cerr << "get_IpeletData()->iSnap.iWithAxes = "
	<< get_IpeletData ()->iSnap.iWithAxes << std::endl;
    std::cerr << "get_IpeletData()->iSnap.iDir = "
    	<< get_IpeletData ()->iSnap.iDir.degrees() << std::endl;
//    get_IpeletData ()->iSnap.iWithAxes = true;
#endif

    //
    //
    if (get_IpeletData()->iSnap.iWithAxes)
      {
	SDG2 sdg;
	double angle = (double) get_IpeletData()->iSnap.iDir;
	Transformation rot (CGAL::ROTATION,
			    Kernel::RT (sin (angle)),
			    Kernel::RT (cos (angle)));
	Transformation inv_rot = rot.inverse ();

	// creating triangulation
	//
	for (auto it = points.begin (); it != points.end (); ++it)
	  {
	    sdg.insert (rot.transform (*it));
	  }
	assert(sdg.is_valid (1));

	switch (fn)
	  {
	  case IPELET_DELAUNAY_OPTION:
	    draw_delaunay_in_ipe(sdg, inv_rot, bbox);
	    break;
	  case IPELET_VORONOI_OPTION:
	    draw_voronoi_in_ipe(sdg, inv_rot, bbox);
	    break;
	  case IPELET_BISECTOR_OPTION:
	    draw_squares_in_ipe(sdg, inv_rot, bbox);
	    break;
	  default:
	    break;
	  }
      }
    else
      {
	switch (fn)
	  {
	  case IPELET_DELAUNAY_OPTION:
	    for_each_angle (points, bbox,
			    [this] (const SDG2& g, const Transformation& inv,
				const Iso_rectangle_2 &bbox)
			      { draw_delaunay_in_ipe(g, inv, bbox);},
			    1.0);
	    break;
	  case IPELET_VORONOI_OPTION:
	    for_each_angle (points, bbox,
			    [this] (const SDG2& g, const Transformation& inv,
				const Iso_rectangle_2 &bbox)
			      { draw_voronoi_in_ipe(g, inv, bbox);},
			    1.0);
	    break;
	  case IPELET_BISECTOR_OPTION:
	    for_each_angle (points, bbox,
			    [this] (const SDG2& g, const Transformation& inv,
				const Iso_rectangle_2 &bbox)
			      { draw_voronoi_in_ipe(g, inv, bbox);},
			    1.0);
	    break;
	  default:
	    break;
	  }
      };
    }

  void
  Unoriented_Linfty_Geometry_ipelet::draw_delaunay_in_ipe (
      const SDG2& g, const Transformation& inv,
      const Iso_rectangle_2 &bbox) const
  {
    std::vector<Segment_2> segments;
    for (auto eit = g.finite_edges_begin (); eit != g.finite_edges_end ();
	++eit)
      {
	SDG2::Edge e = *eit;
	Segment_2 s (
	    inv.transform (
		e.first->vertex (g.ccw (e.second))->site ().point ()),
	    inv.transform (
		e.first->vertex (g.cw (e.second))->site ().point ()));
	segments.push_back (s);
      }
    Ipelet_base::draw_in_ipe (segments.begin (), segments.end (), true, true);
  }

  void
  Unoriented_Linfty_Geometry_ipelet::draw_voronoi_in_ipe (
      const SDG2& g, const Transformation& inv,
      const Iso_rectangle_2 &bbox) const
  {
    // obtaining
    //
    Stream voronoi (inv);
    g.draw_dual (voronoi);

    std::vector<Segment_2> seg_cont;

    // filter degenerate segments
    //
    for (auto iteS = voronoi.seg_list.begin (); iteS != voronoi.seg_list.end ();)
      {
	auto itc = iteS++;
	if (itc->is_degenerate ())
	  voronoi.seg_list.erase (itc);
      }

    get_IpePage ()->deselectAll ();

    // cast rays into segments in bbox
    //
    cast_into_seg (voronoi.ray_list.begin (), voronoi.ray_list.end (), bbox,
		   std::back_inserter (seg_cont));

    // cast lines into segments in bbox
    //
    cast_into_seg (voronoi.line_list.begin (), voronoi.line_list.end (), bbox,
		   std::back_inserter (seg_cont));

    // cast lines into segments in bbox
    //
    cast_into_seg (voronoi.seg_list.begin (), voronoi.seg_list.end (), bbox,
		   std::back_inserter (seg_cont));

    Ipelet_base::draw_in_ipe (seg_cont.begin (), seg_cont.end (), true, true);
  }

  void
  Unoriented_Linfty_Geometry_ipelet::draw_squares_in_ipe (
      const SDG2& g, const Transformation& inv,
      const Iso_rectangle_2 &bbox) const
  {
    Stream stream(inv);

    for (auto eit = g.finite_edges_begin(); eit != g.finite_edges_end(); ++ eit)
      {
	g.draw_dual_edge(*eit, stream);
      }

    get_IpePage ()->deselectAll ();

    std::vector<Segment_2> seg_cont;

    // cast rays into segments in bbox
    //
    cast_into_seg (stream.ray_list.begin (), stream.ray_list.end (), bbox,
		   std::back_inserter (seg_cont));

    // cast lines into segments in bbox
    //
    cast_into_seg (stream.line_list.begin (), stream.line_list.end (), bbox,
    		   std::back_inserter (seg_cont));

    // cast lines into segments in bbox
    //
    cast_into_seg (stream.seg_list.begin (), stream.seg_list.end (), bbox,
		   std::back_inserter (seg_cont));

    Ipelet_base::draw_in_ipe (seg_cont.begin (), seg_cont.end (), true, true);
  }

  template<typename DrawingFunction>
    void
    Unoriented_Linfty_Geometry_ipelet::for_each_angle (
	const std::vector<Point_2> &points, const Iso_rectangle_2 &bbox,
	DrawingFunction f, double angle_inc) const
    {
      SDG2 sdg;
      ipe::Page *ipe_page = get_IpePage ();
      int view = ipe_page->countViews ();
      const int init_layer_id = get_IpeletData ()->iLayer;
      const ipe::String init_layer_name = ipe_page->layer (init_layer_id);

      for (double angle = 0; angle <= 90; angle += angle_inc, view++)
	{
	  ipe::Angle ipe_angle = ipe::Angle::Degrees (angle);
	  Transformation rot (CGAL::ROTATION,
			      Kernel::RT (sin ((double) ipe_angle)),
			      Kernel::RT (cos ((double) ipe_angle)));
	  Transformation inv_rot = rot.inverse ();

	  // creating triangulation
	  //
	  for (auto it = points.begin (); it != points.end (); ++it)
	    {
	      sdg.insert (rot.transform (*it));
	    }
	  assert(sdg.is_valid (1));

	  // setup layer
	  //
	  ipe::String layer_name;
	  ipe::StringStream layerNameS (layer_name);
	  layerNameS << "angle_" << angle;

	  ipe_page->addLayer (layer_name);
	  ipe_page->insertView (view, layer_name);
	  ipe_page->setVisible (view, layer_name, true);
	  ipe_page->setVisible (view, init_layer_name, true);
	  ipe_page->setActive (view, init_layer_name);
	  get_IpeletData ()->iLayer = ipe_page->findLayer (layer_name);

#ifdef NDEBUG
	  std::cerr << "iLayer = " << get_IpeletData ()->iLayer << std::endl;
#endif

	  // draw object
	  //
	  f(sdg, inv_rot, bbox);

	  // get triangulation ready for next angle
	  //
	  sdg.clear ();
	}
    }

  template<class T, class output_iterator>
    void
    Unoriented_Linfty_Geometry_ipelet::cast_into_seg (
	const T& obj, const Iso_rectangle_2& bbox, output_iterator out_it) const
    {
      CGAL::Object obj_cgal = CGAL::intersection (obj, bbox);
      Segment_2 s;
      if (CGAL::assign (s, obj_cgal))
	{
	  *out_it++ = s;
	}
    }

  template<class iterator, class output_iterator>
    void
    Unoriented_Linfty_Geometry_ipelet::cast_into_seg (
	const iterator begin, const iterator end, const Iso_rectangle_2& bbox,
	output_iterator out_it) const
    {
      for (iterator it = begin; it != end; ++it)
	{
	  cast_into_seg (*it, bbox, out_it);
	}
    }

} // namespace unoriented_linfty

// register the ipelet in Ipe
//
CGAL_IPELET(unoriented_linfty::Unoriented_Linfty_Geometry_ipelet);
