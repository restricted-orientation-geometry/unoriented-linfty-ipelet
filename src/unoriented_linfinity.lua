--[
-- This file is part of unoriented-linfty-ipelet.
--
-- Copyright 2017 Carlos Alegría Galicia
--
-- unoriented-linfty-ipelet is free software: you can redistribute it
-- and/or modify it under the terms of the GNU General Public License
-- as published by the Free Software Foundation, either version 3 of
-- the License, or any later version.
--
-- unoriented-linfty-ipelet is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with unoriented-linfty-ipelet. If not, see
-- <http://www.gnu.org/licenses/>.
--]

----------------------------------------------------------------------
-- CGAL Hulls ipelet description
----------------------------------------------------------------------

label = "Unoriented Linfty Graphs"

about = [[
Creates several proximity graphs of a set of points in the L-Infinity
metric
]]

-- this variable will store the C++ ipelet when it has been loaded
ipelet = false

function run(model, num)
  if not ipelet then ipelet = assert(ipe.Ipelet(dllname)) end
  model:runIpelet(methods[num].label, ipelet, num)
end

methods = {
  { label="Delaunay" },
  { label="Voronoi" },
  { label="Squares" },
  { label="Help" },
}

----------------------------------------------------------------------